/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/app.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/app.js":
/*!***********************!*\
  !*** ./src/js/app.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("const Utils = __webpack_require__(/*! ./utils */ \"./src/js/utils.js\");\n\nconst CommentsListComponent = __webpack_require__(/*! ./comments.component */ \"./src/js/comments.component.js\");\n\nnew CommentsListComponent();\n\nnew Utils();\n\n\n//# sourceURL=webpack:///./src/js/app.js?");

/***/ }),

/***/ "./src/js/comments.component.js":
/*!**************************************!*\
  !*** ./src/js/comments.component.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("const Utils = __webpack_require__(/*! ./utils */ \"./src/js/utils.js\");\n\nclass CommentsListComponent extends HTMLElement {\n    static get tag() {\n        return \"t-comments-list\";\n      }\n \n    constructor() {\n        super();\n        this.utils = new Utils();\n    }\n\n    domReady(fn) {\n        if (document.readyState != 'loading') {\n            fn();\n        } else if (document.addEventListener) {\n            document.addEventListener('DOMContentLoaded', fn);\n        } else {\n            document.attachEvent('onreadystatechange', () => {\n                if (document.readyState != 'loading')\n                    fn();\n            });\n        }\n    }\n\n    getComments() {\n        fetch('https://my-json-server.typicode.com/telegraph/front-end-exercise-contractors/comments')\n            .then( (response) => {\n                return response.json();\n            })\n            .then( (commentsData) => {\n                this.render(commentsData);\n                this.bindSortLikesButtonEvents(commentsData);\n            });\n    }\n\n    connectedCallback() {\n        this.domReady(this.getComments.bind(this));\n    }\n\n    bindSortLikesButtonEvents(data) {\n        let sortLikesButton = document.querySelector('.sort-likes-button');\n        sortLikesButton.addEventListener('click', (event) => {\n            this.utils.sortDataDesc(data);\n            this.render(data);\n        });\n    }\n\n    render(data) {\n        this.innerHTML = `\n        <div id=\"comments\" class=\"comments-container\">\n            <div class=\"comments-filters\">\n                <div class=\"comments-filters-title\">\n                    <img src=\"static/images/comment-box.svg\" class=\"comment-box-image\" alt=\"\">\n                    <span>${data.length} Comments</span>\n                </div>\n                <div class=\"comments-filters-buttons\">\n                    <span>Sort</span>\n                    <button class=\"sort-likes-button\">Likes</button>\n                </div>\n            </div>\n            <section class=\"comments-widgets\">\n\n            ${data.map((comment) => {\n                return `<div class=\"comment-row\">\n                    <div class=\"comment-header\">\n                        <div class=\"comment-author\">${comment.name}</div>\n                        <div class=\"comment-likes\"> <span class=\"comment-likes-count\">${comment.likes} Likes</span></div>\n                    </div>\n                    <div class=\"comment-text\">${comment.body}</div>\n            </div>`\n    \n            }).join(\"\")}\n            </section>\n        </div>\n        `;\n    }\n}\n\ncustomElements.define(CommentsListComponent.tag, CommentsListComponent);\n\nmodule.exports = CommentsListComponent;\n\n//# sourceURL=webpack:///./src/js/comments.component.js?");

/***/ }),

/***/ "./src/js/utils.js":
/*!*************************!*\
  !*** ./src/js/utils.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("/**\n * Dummy utility class to demonstrate a basic JS\n * structure and assoiciated test\n * @param {Object} params - containing:\n * @param {String} homePagePath - the pathname of the homepage (defaults to '/')\n */\nclass Utils {\n\tconstructor(params) {\n\t\tthis.params = Object.assign({\n\t\t\thomePagePath: '/'\n\t\t}, params);\n\t}\n\n\t/**\n\t * Is the user on the hompage\n\t * @return {Boolean}\n\t */\n\tisHomePage() {\n\t\treturn document.location.pathname === this.params.homePagePath;\n\t}\n\n\tasync getComments() {\n\t\ttry {\n\t\t\tconst result = await fetch(\n\t\t\t\t`https://my-json-server.typicode.com/telegraph/front-end-exercise-contractors/comments`\n\t\t\t);\n\t\t\tconst data = await result.json();\n\t\t\treturn data.rates[destination];\n\t\t} catch (e) {\n\t\t\treturn null;\n\t\t}\n\t}\n\n\tsortDataDesc(data) {\n\t\treturn data.sort((a, b) => (a.likes > b.likes) ? -1 : 1)\n\t}\t\n}\n\nmodule.exports = Utils;\n\n//# sourceURL=webpack:///./src/js/utils.js?");

/***/ })

/******/ });