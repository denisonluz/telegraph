const Utils = require("./utils");

const dummyCommentsData = [
	{
	  "id": 1,
	  "date": "2019-04-23T22:26:43.511Z",
	  "name": "Dawud Esparza",
	  "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sed gravida orci.",
	  "likes": 33
	},
	{
	  "id": 2,
	  "date": "2019-04-23T19:26:41.511Z",
	  "name": "Lennie Wainwright",
	  "body": "Quisque maximus augue ut ex tincidunt sodales. Nullam interdum consectetur mi at pellentesque.",
	  "likes": 4
	},
	{
	  "id": 3,
	  "date": "2019-04-23T18:26:48.511Z",
	  "name": "Mindy Sykes",
	  "body": "Nam sit amet diam rutrum, venenatis est ac, tempus massa. Etiam tempus libero sit amet bibendum lacinia. Quisque ligula dolor, venenatis quis urna non, tristique laoreet erat.",
	  "likes": 58
	},
	{
	  "id": 4,
	  "date": "2019-04-24T08:23:49.511Z",
	  "name": "Arianne Ashton",
	  "body": "Vivamus sit amet turpis nulla. Mauris rhoncus nulla in lobortis rhoncus.",
	  "likes": 91
	},
	{
	  "id": 5,
	  "date": "2019-04-24T07:26:42.511Z",
	  "name": "Courteney Moreno",
	  "body": "Mauris ut dolor ipsum. Phasellus imperdiet massa a dui imperdiet dignissim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.",
	  "likes": 7
	}
  ];

global.fetch = jest.fn(() =>
	Promise.resolve({
		json: () => Promise.resolve(dummyCommentsData),
	})
);

beforeEach(() => {
	fetch.mockClear();
});

describe('Utils', () => {
	describe('isHomePage', () => {
		it("should match if on homepage", () => {
			const utils = new Utils();
			window.history.pushState({}, "", "/");
			expect(utils.isHomePage()).toBeTruthy();
		});

		it("should match if on homepage and has query parameters", () => {
			const utils = new Utils();
			window.history.pushState({}, "", "/?foo=bar");
			expect(utils.isHomePage()).toBeTruthy();
		});

		it("should match if on homepage and has hash navigation", () => {
			const utils = new Utils();
			window.history.pushState({}, "", "/#foo");
			expect(utils.isHomePage()).toBeTruthy();
		});

		it("should not match if on another page", () => {
			const utils = new Utils();
			window.history.pushState({}, '', '/news');
			expect(utils.isHomePage()).toBeFalsy();
		});

		it("should match with different homepage parameter", () => {
			const utils = new Utils({
				homePagePath: '/home'
			});
			window.history.pushState({}, "", "/home");
			expect(utils.isHomePage()).toBeTruthy();
		});

		it("feth comments on load", async () => {
			const utils = new Utils();
			const comments = await utils.getComments();
			expect(fetch).toHaveBeenCalledTimes(1);
		});

	});
});