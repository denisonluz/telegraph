const CommentsListComponent = require("./comments.component");
const Utils = require("./utils");

const mockCommentsData = [{
		"id": 1,
		"date": "2019-04-23T22:26:43.511Z",
		"name": "Dawud Esparza",
		"body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sed gravida orci.",
		"likes": 33
	},
	{
		"id": 2,
		"date": "2019-04-23T19:26:41.511Z",
		"name": "Lennie Wainwright",
		"body": "Quisque maximus augue ut ex tincidunt sodales. Nullam interdum consectetur mi at pellentesque.",
		"likes": 4
	},
	{
		"id": 3,
		"date": "2019-04-23T18:26:48.511Z",
		"name": "Mindy Sykes",
		"body": "Nam sit amet diam rutrum, venenatis est ac, tempus massa. Etiam tempus libero sit amet bibendum lacinia. Quisque ligula dolor, venenatis quis urna non, tristique laoreet erat.",
		"likes": 58
	},
	{
		"id": 4,
		"date": "2019-04-24T08:23:49.511Z",
		"name": "Arianne Ashton",
		"body": "Vivamus sit amet turpis nulla. Mauris rhoncus nulla in lobortis rhoncus.",
		"likes": 91
	},
	{
		"id": 5,
		"date": "2019-04-24T07:26:42.511Z",
		"name": "Courteney Moreno",
		"body": "Mauris ut dolor ipsum. Phasellus imperdiet massa a dui imperdiet dignissim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.",
		"likes": 7
	}
];

describe('Show comments', () => {

	it("home page should return comments", () => {
		const commentsListComponent = new CommentsListComponent();
		const utils = new Utils();

		commentsListComponent.render(mockCommentsData);

		expect(utils.isHomePage()).toBeTruthy();

		expect(commentsListComponent.innerHTML).toBeDefined();
	});

	it("each comment should show the username", () => {
		const commentsListComponent = new CommentsListComponent();
		const utils = new Utils();

		commentsListComponent.render(mockCommentsData);

		expect(utils.isHomePage()).toBeTruthy();

		Array.from(commentsListComponent.getElementsByClassName('comment-author')).forEach((element, index) => {
			expect(element.innerHTML === mockCommentsData[index].name).toBeTruthy();
		});
	});

	it("And each comment should show the comment body", () => {
		const commentsListComponent = new CommentsListComponent();
		const utils = new Utils();

		commentsListComponent.render(mockCommentsData);

		expect(utils.isHomePage()).toBeTruthy();

		Array.from(commentsListComponent.getElementsByClassName('comment-text')).forEach((element, index) => {
			expect(element.innerHTML === mockCommentsData[index].body).toBeTruthy();
		});
	});

	it("And each comment should show the like count", () => {
		const commentsListComponent = new CommentsListComponent();
		const utils = new Utils();

		commentsListComponent.render(mockCommentsData);

		expect(utils.isHomePage()).toBeTruthy();

		Array.from(commentsListComponent.getElementsByClassName('comment-likes-count')).forEach((element, index) => {
			expect(element.innerHTML === mockCommentsData[index].likes + ' Likes').toBeTruthy();
		});
	});
});

describe('Order comments by likes', () => {

	it("the comments should display in order of most likes", () => {
		const commentsListComponent = new CommentsListComponent();
		const utils = new Utils();

		const sortedData = utils.sortDataDesc(mockCommentsData);
		
		commentsListComponent.render(sortedData);

		expect(utils.isHomePage()).toBeTruthy();

		Array.from(commentsListComponent.getElementsByClassName('comment-likes-count')).forEach((element, index) => {
			expect(element.innerHTML === sortedData[index].likes + ' Likes').toBeTruthy();
		});
	});

	it("the comments should show most liked first", () => {
		const commentsListComponent = new CommentsListComponent();
		const utils = new Utils();

		const orginalLikeCountData = []

		const sortedData = utils.sortDataDesc(mockCommentsData);

		mockCommentsData.map((comment) => {
			orginalLikeCountData.push(comment.likes)
		})
		
		commentsListComponent.render(sortedData);

		const firstCommentLikesCount =  commentsListComponent.getElementsByClassName('comment-likes-count')[0];

		expect(utils.isHomePage()).toBeTruthy();

		expect(firstCommentLikesCount.innerHTML === ( (Math.max.apply(null, orginalLikeCountData)) + ' Likes' ) ).toBeTruthy();

	}); 
  });
