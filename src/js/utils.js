/**
 * Dummy utility class to demonstrate a basic JS
 * structure and assoiciated test
 * @param {Object} params - containing:
 * @param {String} homePagePath - the pathname of the homepage (defaults to '/')
 */
class Utils {
	constructor(params) {
		this.params = Object.assign({
			homePagePath: '/'
		}, params);
	}

	/**
	 * Is the user on the hompage
	 * @return {Boolean}
	 */
	isHomePage() {
		return document.location.pathname === this.params.homePagePath;
	}

	async getComments() {
		try {
			const result = await fetch(
				`https://my-json-server.typicode.com/telegraph/front-end-exercise-contractors/comments`
			);
			const data = await result.json();
			return data.rates[destination];
		} catch (e) {
			return null;
		}
	}

	sortDataDesc(data) {
		return data.sort((a, b) => (a.likes > b.likes) ? -1 : 1)
	}	
}

module.exports = Utils;