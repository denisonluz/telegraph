const Utils = require("./utils");

class CommentsListComponent extends HTMLElement {
    static get tag() {
        return "t-comments-list";
      }
 
    constructor() {
        super();
        this.utils = new Utils();
    }

    domReady(fn) {
        if (document.readyState != 'loading') {
            fn();
        } else if (document.addEventListener) {
            document.addEventListener('DOMContentLoaded', fn);
        } else {
            document.attachEvent('onreadystatechange', () => {
                if (document.readyState != 'loading')
                    fn();
            });
        }
    }

    getComments() {
        fetch('https://my-json-server.typicode.com/telegraph/front-end-exercise-contractors/comments')
            .then( (response) => {
                return response.json();
            })
            .then( (commentsData) => {
                this.render(commentsData);
                this.bindSortLikesButtonEvents(commentsData);
            });
    }

    connectedCallback() {
        this.domReady(this.getComments.bind(this));
    }

    bindSortLikesButtonEvents(data) {
        let sortLikesButton = document.querySelector('.sort-likes-button');
        sortLikesButton.addEventListener('click', (event) => {
            this.utils.sortDataDesc(data);
            this.render(data);
        });
    }

    render(data) {
        this.innerHTML = `
        <div id="comments" class="comments-container">
            <div class="comments-filters">
                <div class="comments-filters-title">
                    <img src="static/images/comment-box.svg" class="comment-box-image" alt="">
                    <span>${data.length} Comments</span>
                </div>
                <div class="comments-filters-buttons">
                    <span>Sort</span>
                    <button class="sort-likes-button">Likes</button>
                </div>
            </div>
            <section class="comments-widgets">

            ${data.map((comment) => {
                return `<div class="comment-row">
                    <div class="comment-header">
                        <div class="comment-author">${comment.name}</div>
                        <div class="comment-likes"> <span class="comment-likes-count">${comment.likes} Likes</span></div>
                    </div>
                    <div class="comment-text">${comment.body}</div>
            </div>`
    
            }).join("")}
            </section>
        </div>
        `;
    }
}

customElements.define(CommentsListComponent.tag, CommentsListComponent);

module.exports = CommentsListComponent;